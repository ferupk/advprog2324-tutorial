package id.ac.ui.cs.advprog.eshop.service;

import id.ac.ui.cs.advprog.eshop.model.Product;

import java.util.List;

public interface ProductService {
    public Product create(Product product);
    public void edit(String targetId, Product editValues);
    public void delete(String targetId);
    public List<Product> findAll();
    public Product findById(String targetId);
}
